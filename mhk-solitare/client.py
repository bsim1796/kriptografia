'''
Budai Samuel
bsim1796

P2p chat systems client
 
All clients are unique because they get their unique port number from the keyserver
Lifecycle:
1. Clients register their usernames and public key to the key server, they got their port number
2. Client can ask the server for users, a specific users adress and can exit
3. After getting the peers adress, they close the connection with the keyServer and start to communicate with the peer

'''

import socket
import sys
from _thread import *
from knapsack import create_public_key, generate_private_key

IP = 'localhost'
PORT = 17960
BUFS = 4096

peerPort = 0
peerKey = ""

private_key = ""
public_key = ""

def console(conn):
  try:
    while 1:
      data = conn.recv(BUFS)
      data = data.decode('utf_8')
      print(data)
  except:
    exit()

def initPeerConsole(conn):
  try:
    while 1:
      data = conn.recv(BUFS)
      data = data.decode('utf_8')
      if data == "The adress:":
        data = conn.recv(BUFS)
        peerPort = data.decode('utf_8')
        print("Port is ", peerPort)
        data = conn.recv(BUFS)
        peerKey = data.decode('utf_8')
        print("Key is ", peerKey)
        conn.close()
      print(data)
  except:
    exit()

def generateKanpsackKeyPair():
  private_key = generate_private_key()
  public_key = create_public_key(private_key)

conn = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
conn.connect((IP, PORT))

username = input("Enter your username ")
conn.sendall(username.encode('utf-8'))  
data = conn.recv(BUFS)
data = data.decode('utf_8')
while data != "OK":
	print(data)
	username = input("Enter your username "); 
	conn.sendall(username.encode('utf-8')) 
	data = conn.recv(BUFS)
	data = data.decode('utf_8')
generateKanpsackKeyPair()
conn.sendall(public_key.encode('utf-8'))

print('Welcome to our chatroom! \n')
print('Rules:\n To see the active users type users')
print('To send a private mesage type Adress then ENTER then <username> then ENTER again then the <message>. The keyServer will give you the')
print(' targets adress andr everiting you type after that will be redirected to the target')

start_new_thread(initPeerConsole,(conn,))

while 1:
  message = input() 
  packet = username + ": " + message
  print("sent ", packet)
  conn.sendall(packet.encode('utf-8'))
  if message == "exit":
    break

conn = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
conn.connect((IP, peerPort))

start_new_thread(console,(conn,))

while 1:
  message = input() 
  packet = username + ": " + message
  conn.sendall(packet.encode('utf-8'))
  if message == "exit":
    break

conn.close()

sys.exit() 
