import utils
import string
import random

def generate_private_key(n=8):
    w = []
    w = [0 for i in range(n)]
    w[0] = random.randint(2,10)
    for i in range(1,n):
        total = 0
        for j in range(0,i):
            total=total + w[j]
        w[i] = random.randint(total + 1, 2 * total)
    total = 0
    for j in range(n):
        total=total + w[j]
    q = random.randint(total + 1, 2 * total)
    r=0
    while True:
        r = random.randint(2,q-1)
        if utils.coprime(r,q):
            break
    return (tuple(w),q,r)


def create_public_key(private_key):
    n = len(private_key[0])
    beta = []
    beta = [0 for i in range(n)]
    for i in range(n):
        beta[i] = ( private_key[2] * private_key[0][i] ) % private_key[1]
    return tuple(beta)

def encrypt_mh(message, public_key):
    n = len(message)
    nBeta = len(public_key)
    chiper_message = []
    chiper_message = [0 for i in range(n)]
    for i in range(n):
        alfa = utils.byte_to_bits(ord(message[i]))
        c=0
        for j in range(nBeta):
            c = c + (alfa[j] * public_key[j])
        chiper_message[i] = c
    return tuple(chiper_message)


def decrypt_mh(message, private_key):
    w = private_key[0]
    q = private_key[1]
    r = private_key[2]
    r_ = utils.modinv(r,q)
    n = len(message)
    n_w = len(w)
    plain_message = []
    plain_message = [0 for i in range(n)]
    for i in range(0,n):
        c = (message[i] * r_) % q
        X = []
        while c > 0:
            k = w[n-1]
            for j in reversed(range(0,n_w)):
                if w[j] <= c:
                    X.append(j+1)
                    k = w[j]
                    c = c - k
                    break
        char = 0
        for z in range(len(X)):
            char = char + 2 ** (n_w-X[z])
        plain_message[i] = chr(char)
    result = ""
    return result.join(plain_message)

def test():
    prk = generate_private_key()
    pk  = create_public_key(prk)
    message = "Helloka"
    cripted = encrypt_mh(message,pk)
    print(cripted)
    decripted = decrypt_mh(cripted,prk)
    print(decripted)
