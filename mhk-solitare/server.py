'''
Budai Samuel 
bsim1796

KeyServer for a p2p chat system 

The server can threat any number of user. The server stores the connectiont, username, port ant the public key
The lifecycle:
1. Clients log in their unique username
2. The system allocates an unnique port number to them
3. The clients can:
	- Ask for users: The server sends all users name to the client
	- Ask for a specific uses adress: The server sends that users port number and his public key. After this, the client dont comunicates whit the server anymore.
	- Exit

'''
import socket 
import sys
import _thread

IP = '127.0.0.1'
PORT = 17960
acc = 1
BUFFER_SIZE = 1024

connections = []
usernames = []
ports = []
publicKeys = []

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind((IP, PORT))
s.listen(1)

def t(conn,addr):
	
	name = conn.recv(BUFFER_SIZE) #username / userId. 
	name = name.decode('utf_8')
	isUsed = True
	while isUsed == True:
		isUsed = False
		for usr in usernames:
			if name == usr:
				isUsed = True
				conn.sendall(b'This username is already in use')
				name = conn.recv(BUFFER_SIZE)
				name = name.decode('utf_8')
	conn.sendall(b'OK')
	print(name)
	usernames.append(name) 
	connections.append(conn)
	global acc
	personalPort = PORT + acc
	acc = acc + 1
	ports.append(str(personalPort))

	publicKey = conn.recv(BUFFER_SIZE)
	publicKey = publicKey.decode('utf_8')
	print(publicKey)
	publicKeys.append(publicKey)

	
	while 1:
		message = conn.recv(BUFFER_SIZE)
		message = message.decode('utf_8')
		z = "%s: users" % (name)
		k = "%s: Adress" % (name)
		exit = "%s: exit" % (name)
		if message == exit:
			index = usernames.index(name)
			del usernames[index]
			del connections[index]
			del ports[index]
			del publicKeys[index]
			conn.close()
			break
		if message == z:
			conn.sendall(b'Logged in users are the following:\n')
			for user in usernames:
				conn.sendall(user.encode('utf-8'))
				if user == name:
  					conn.sendall(b' (you)')
				conn.sendall(b'\n')
		elif message == k:
			message = conn.recv(BUFFER_SIZE)
			message = message.decode('utf_8')
			k = "%s: " % (name)
			to = message.split(k,1)[1] 
			index = usernames.index(to)

			conn.sendall(b"The adress:") ######################
			#first the port
			peerPort = ports[index]
			conn.sendall(peerPort.encode('utf_8')) ######################
			# then the public key
			peerKey = publicKeys[index]
			conn.sendall(peerKey.encode('utf_8')) #######################
			del usernames[index]
			del connections[index]
			del ports[index]
			del publicKeys[index]
			conn.close()

while 1: 
	conn, addr = s.accept()
    	
	print('New user logged in: ', addr)
	
	_thread.start_new_thread(t,(conn,addr))

s.close()


