import string, sys

def toNumber(c):
    if c in string.ascii_letters:
        return ord(c.upper()) - 64
    return 24

def toChar(n):
    return chr((n-1)%26+65)

# Joker A is 53 
# Joker B is 54

def down1(deck, card):
    n = deck.index(card)
    if n < 53: 
        deck[n], deck[n+1] = deck[n+1], deck[n]
    else:
        deck[1:] = deck[-1:] + deck[1:-1]

def tripleCut(deck):
    a, b = deck.index(53), deck.index(54)
    if a > b:
        a, b = b, a
    deck[:] = deck[b+1:] + deck[a:b+1] + deck[:a]

def countCut(deck,n):
    n = min(n, 53)
    deck[:-1] = deck[n:-1] + deck[:n]

def oneRound(deck):
    down1(deck,53)
    down1(deck,54)
    down1(deck,54)
    tripleCut(deck)
    countCut(deck,deck[-1])

def output(deck):
    while 1:
        oneRound(deck)
        topCard = min(deck[0], 53)  # both joker is 53
        if deck[topCard] < 53:  # joker no 
            return deck[topCard]

def initSolitaireDeck(commonkey):
    # initializes a solitiare card deck. we start with an ordered deck, and with a long enough string, and when we encrypt that we will get a
    # deck what is "randomized" enough
    deck1 = [23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22]
    iDontCare = encrypt_solitaire(commonkey,deck1)
    return deck1

def encrypt_solitaire(message, deck):
    ascii_message = []
    n = len(message)
    for i in range(n):
        ascii_message.append(toNumber(message[i]))
    
    deck_numbers = []
    for i in range(n):
        deck_numbers.append(output(deck))

    chyper_text = []
    for i in range(n):
        chyper_text.append((deck_numbers[i] + ascii_message[i]) % 26)

    chyper_text_ = []
    for i in range(n):
        chyper_text_.append(toChar(chyper_text[i]))

    return "".join(chyper_text_)

# exactly the same but with minus
def decrypt_solitaire(message, deck):
    ascii_message = []
    n = len(message)
    for i in range(n):
        ascii_message.append(toNumber(message[i]))
    
    deck_numbers = []
    for i in range(n):
        deck_numbers.append(output(deck))
    
    chyper_text = []
    for i in range(n):
        chyper_text.append((ascii_message[i] - deck_numbers[i]) % 26)

    chyper_text_ = []
    for i in range(n):
        chyper_text_.append(toChar(chyper_text[i]))

    return "".join(chyper_text_)
