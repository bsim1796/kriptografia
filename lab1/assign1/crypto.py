"""Assignment 1: Cryptography for CS41 Winter 2020.

Name: Budai Sámuel
SUNet: bsim1796

Replace this placeholder text with a description of this module.
"""
import utils
import string
import random
#################
# CAESAR CIPHER #
#################

def encrypt_caesar(plaintext):
    shift=3
    n=len(plaintext)
    letters = string.ascii_uppercase
    ciphertext = []
    ciphertext = [0 for i in range(n)]
    for i in range(0,n):
        if plaintext[i] in letters:
            k = letters.index(plaintext[i])
            k = (k+shift) % 26
            ciphertext[i] = letters[k]
    return "".join(ciphertext)
    
def decrypt_caesar(ciphertext):
    shift=3
    n=len(ciphertext)
    letters = string.ascii_uppercase
    plaintext = []
    plaintext = [0 for i in range(n)]
    for i in range(0,n):
        if ciphertext[i] in letters:
            k = letters.index(ciphertext[i])
            k = (k+(26-shift)) % 26
            plaintext[i] = letters[k]
    return "".join(plaintext)


###################
# VIGENERE CIPHER #
###################

def encrypt_vigenere(plaintext, keyword):
    nText=len(plaintext)
    nKey=len(keyword)
    letters = string.ascii_uppercase
    ciphertext = []
    ciphertext = [0 for i in range(nText)]
    keyIndex=0
    for i in range(0,nText):
        kKey = letters.index(keyword[keyIndex%nKey])
        kText = letters.index(plaintext[i])
        k = (kKey + kText) % 26
        ciphertext[i] = letters[k]
        keyIndex = keyIndex + 1
    return "".join(ciphertext)

def decrypt_vigenere(ciphertext, keyword):
    nText=len(ciphertext)
    nKey=len(keyword)
    letters = string.ascii_uppercase
    plaintext = []
    plaintext = [0 for i in range(nText)]
    keyIndex=0
    for i in range(0,nText):
        kKey = letters.index(keyword[keyIndex%nKey])
        kText = letters.index(ciphertext[i])
        k = (kText + (26-kKey)) % 26
        plaintext[i] = letters[k]
        keyIndex = keyIndex + 1
    return "".join(plaintext)

#SKYTALE cipher

def encrypt_scytale(plaintext, circumference):
    n=len(plaintext)
    ciphertext=[]
    ciphertext=[0 for i in range(n)]
    j=0
    for k in range(0,n):
        ciphertext[k] = plaintext[j]
        j=j+circumference
        if j>=n:
            j=(j%n) + 1
    return "".join(ciphertext)


def decrypt_scytale(ciphertext, circumference):
    n=len(ciphertext)
    plaintext=[]
    plaintext=[0 for i in range(n)]
    j=0
    for k in range(0,n):
        plaintext[k] = ciphertext[j]
        j=j+circumference - 1
        if j>=n:
            j=(j%n) + 1
    return "".join(plaintext)

#RAILFENCE cipher
def makeRail(str, column, row): 
    rail=[[0 for x in range(column)] for y in range(row)] 
    z=0
    for i in range(0,row):
        for j in range(0,column):
            rail[i][j] = str[z]
            z=z+1
        z=z-1
    return rail

def encrypt_railfence(plaintext, num_rails):
    n=len(plaintext)
    ciphertext=[]
    ciphertext=[0 for i in range(n)]
    j=0
    rail=makeRail(plaintext,num_rails,)
        
            

    return "".join(ciphertext)

def decrypt_railfence(ciphertext, num_rails):
    n=len(ciphertext)
    plaintext=[]
    plaintext=[0 for i in range(n)]
    j=0
    for k in range(0,n):
        plaintext[k] = ciphertext[j]
        j=j+(num_rails*2-2) -1
        if j>=n:
            j=(j%n) + 1
    return "".join(plaintext)

########################################
# MERKLE-HELLMAN KNAPSACK CRYPTOSYSTEM #
########################################

def generate_private_key(n=8):
    w = []
    w = [0 for i in range(n)]
    w[0] = random.randint(2,10)
    for i in range(1,n):
        total = 0
        for j in range(0,i):
            total=total + w[j]
        w[i] = random.randint(total + 1, 2 * total)
    total = 0
    for j in range(n):
        total=total + w[j]
    q = random.randint(total + 1, 2 * total)
    r=0
    while True:
        r = random.randint(2,q-1)
        if utils.coprime(r,q):
            break
    return (tuple(w),q,r)


def create_public_key(private_key):
    n = len(private_key[0])
    beta = []
    beta = [0 for i in range(n)]
    for i in range(n):
        beta[i] = ( private_key[2] * private_key[0][i] ) % private_key[1]
    return tuple(beta)

def encrypt_mh(message, public_key):
    n = len(message)
    nBeta = len(public_key)
    chiper_message = []
    chiper_message = [0 for i in range(n)]
    for i in range(n):
        alfa = utils.byte_to_bits(ord(message[i]))
        c=0
        for j in range(nBeta):
            c = c + (alfa[j] * public_key[j])
        chiper_message[i] = c
    return tuple(chiper_message)


def decrypt_mh(message, private_key):
    w = private_key[0]
    q = private_key[1]
    r = private_key[2]
    r_ = utils.modinv(r,q)
    n = len(message)
    n_w = len(w)
    plain_message = []
    plain_message = [0 for i in range(n)]
    for i in range(0,n):
        c = (message[i] * r_) % q
        X = []
        while c > 0:
            k = w[n-1]
            for j in reversed(range(0,n_w)):
                if w[j] <= c:
                    X.append(j+1)
                    k = w[j]
                    c = c - k
                    break
        char = 0
        for z in range(len(X)):
            char = char + 2 ** (n_w-X[z])
        plain_message[i] = chr(char)
    result = ""
    return result.join(plain_message)

def test():
    prk = generate_private_key()
    pk  = create_public_key(prk)
    message = "Helloka"
    cripted = encrypt_mh(message,pk)
    print(cripted)
    decripted = decrypt_mh(cripted,prk)
    print(decripted)
