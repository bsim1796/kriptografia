from typing import List

import numpy as np


def construct_matrix(key: str):
    letters: str = 'ABCDEFGHIKLMNOPQRSTUVWXYZ'

    matrix = []
    for char in key.upper():
        if char in letters:
            matrix.append(char)
            letters = letters.replace(char, '')

    for char in letters:
        matrix.append(char)

    return np.array(matrix).reshape((5, 5))


def code_for(matrix, x1, y1, x2, y2):
    if x1 != x2 and y1 != y2:
        a = matrix[x1][y2]
        b = matrix[x2][y1]
    elif x1 == x2:
        a = matrix[x1][y1 + 1] if y1 != 4 else matrix[x1 + 1][0]
        b = matrix[x2][y2 + 1] if y2 != 4 else matrix[x2 + 1][0]
    else:
        a = matrix[x1 + 1][y1] if x1 != 4 else matrix[0][y1 + 1]
        b = matrix[x2 + 1][y2] if x2 != 4 else matrix[0][y2 + 1]

    return a, b

def unique(s):
    return len(set(s)) == len(s)

f = open("aba.txt", "r")
words = []
for x in f:
    if unique(x) and x.find('j')==-1:
        words.append(x.strip())

solutions = []
# TH -> ZN
# DX -> FU
# change the hardcoded values if you want ot find solutions for other rules
for word in words:
    matrix: np.ndarray = construct_matrix(word)

    tx, ty = [a[0] for a in np.where(matrix == 'T')]
    hx, hy = [a[0] for a in np.where(matrix == 'H')]

    dx, dy = [a[0] for a in np.where(matrix == 'D')]
    xx, xy = [a[0] for a in np.where(matrix == 'X')]

    a, b = code_for(matrix, tx, ty, hx, hy)
    c, d = code_for(matrix, dx, dy, xx, xy)

    if a == 'Z' and b == 'N' and c == 'F' and d == 'U':
        print(word)
        print(matrix)
        solutions.append(word)

print(solutions)
